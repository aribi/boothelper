echo "user password:"
$Password = Read-Host -AsSecureString
New-LocalUser "user" -Password $Password -FullName "You Sir"
Add-LocalGroupMember -Group "Remote Desktop Users" -Member "user"
