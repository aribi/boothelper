# README #

Boot scripts for setting up VM's in VU ITvO's SciCloud environment

### What is this repository for? ###

* Publicly accesible resources needed by the boot environment
* Mostly for windows

### How do I use it ? ###

Resources can be downloaded with powershell 
```powershell
Invoke-WebRequest -Uri 'https://bitbucket.org/aribi/boothelper/src/master/windows/viodrivers.powershell' -OutFile
```
